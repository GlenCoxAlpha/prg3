﻿/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class CollisionSelector : MonoBehaviour 
{
	public Collider selectedColl;
	public Collider[] colliders;
	public bool addSelectedColliders = false;
	public bool removeSelectedColliders = false;
	public int selectorID;

	private void selectCollider()
	{
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			selectorID++;
			print ("selection up");
		}

		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			selectorID--;
			print ("selection down");
		}

		if(selectorID >3 || selectorID <0)
		{
			selectorID=0;
			print ("selection reset");
		}

		if(addSelectedColliders == true)
		{
			addSelectedColliders = false;
			applySelection(selectorID);
		}

		if(removeSelectedColliders == true)
		{
			removeSelectedColliders = false;
			removeSelection(selectorID);
		}
	}

	private void applySelection(int selectedID)
	{
		print ("applying selection" + selectedID);
		MeshFilter[] childObjects = gameObject.GetComponentsInChildren<MeshFilter>();
		selectedColl = colliders[selectedID];

		for (int i = 0; i < childObjects.Length; i++)
		{
			childObjects[i].gameObject.AddComponent<selectedColl>();
		}
	}

	private void removeSelection(int selectedID)
	{
		print ("removing selection" + selectedID);
		MeshFilter[] childObjects = gameObject.GetComponentsInChildren<MeshFilter>();
		selectedColl = colliders[selectedID];

		foreach (Collider selectedColl in childObjects)
		{
			Destroy(selectedColl);
		}
	}

	void Start () 
	{
		colliders = new Collider[4];
		colliders[0] = (BoxCollider) GetComponent<BoxCollider>();
		colliders[1] = (CapsuleCollider) GetComponent<CapsuleCollider>();;
		colliders[2] = (MeshCollider) GetComponent<MeshCollider>();
		colliders[3] = (SphereCollider) GetComponent<SphereCollider>();
		selectedColl = new Collider();
	}

	void Update () 
	{
		selectCollider();
	}

}
*/
