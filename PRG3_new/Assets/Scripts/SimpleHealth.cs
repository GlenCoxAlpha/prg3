﻿using UnityEngine;
using System.Collections;

public class SimpleHealth : MonoBehaviour {

	public int health = 10;

	public void takeDamage(int amount)
	{
		health -= amount;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
