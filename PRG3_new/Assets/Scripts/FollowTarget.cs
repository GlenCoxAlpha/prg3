﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour 
{

	public Transform target;
	public float speed;

	public void follow()
	{
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position,
		                                         target.position, step);
	}
	void LateUpdate () 
	{
		follow ();
	}
}
