﻿using UnityEngine;
using System.Collections;

public class VectorPractice : MonoBehaviour {

    public Transform startPoint;
    public Transform endPoint;
    public float worldAxisScale;

	// Use this for initialization
	void Start () {
        Vector3 offset = endPoint.position - startPoint.position;
        print(offset);
        print(offset.magnitude);
        print( Vector3.Distance(startPoint.position, endPoint.position) );
	}
	
    private void debugLines()
    {
        //World Axis Lines
        Debug.DrawLine(Vector3.zero, Vector3.right * worldAxisScale, Color.red); //X axis
        Debug.DrawLine(Vector3.zero, Vector3.up * worldAxisScale, Color.green); //Y Axis
        Debug.DrawLine(Vector3.zero, Vector3.forward * worldAxisScale, Color.blue); //Z axis

        Debug.DrawLine(startPoint.position, endPoint.position, Color.yellow);
    }

	// Update is called once per frame
	void Update () {
        debugLines();
	}
}
