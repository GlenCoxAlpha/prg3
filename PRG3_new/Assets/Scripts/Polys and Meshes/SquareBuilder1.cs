﻿using UnityEngine;
using System.Collections;

public class SquareBuilder1 : MonoBehaviour {

    public int size;        //How big is the shape?
    public Vector3 origin;  //Where to start making the shape
    public Vector3[] vertices = new Vector3[4];
    public Vector2[] uvs;
    public int[] triangles;

	// Use this for initialization
	void Start () {
        getVertices();
        renderSquare();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void getVertices()
    {
        vertices[0] = new Vector3(origin.x + size / 2f, origin.y + size / 2f, 0);
        vertices[1] = new Vector3(origin.x - size / 2f, origin.y - size / 2f, 0);
        vertices[2] = new Vector3(origin.x - size / 2f, origin.y + size / 2f, 0);
        vertices[3] = new Vector3(origin.x + size / 2f, origin.y - size / 2f, 0);
    }

    private void renderSquare()
    {
        Mesh currentMesh = new Mesh();
        currentMesh.vertices = vertices;
        currentMesh.uv = uvs;
        currentMesh.triangles = triangles;
        currentMesh.RecalculateNormals();

        MeshFilter filter = new MeshFilter();
        filter = GetComponent<MeshFilter>();
        filter.mesh = currentMesh;
    }
}
