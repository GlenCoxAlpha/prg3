﻿using UnityEngine;
using System.Collections;

public class SquareBuilder : MonoBehaviour {

    public float size;          //How big is the shape?
    public Vector3 origin;      //Where to start making the shape
    public Vector3[] vertices;  //Store the points of the shape
    public Vector2[] uvs;       //Store mapping data for each point
    public int[] triangles;     //Store order of points to make a triangle

    private float calculateHalfSize()
    {
        return size / 2f; //return what is half of the size
    }

    private void createPoints()
    {
        //store halfSize temporarily for convenience
        float halfSize = calculateHalfSize();
        //Top right point (Based on half size) (+halfSize, +halfSize)
        Vector3 topRight = new Vector3(origin.x + halfSize, 0, origin.z + halfSize);
        //Top left point (Based on half size) (-halfSize, +halfSize)
        Vector3 topLeft = new Vector3(origin.x - halfSize, 0, origin.z + halfSize);
        //Bottom right point (Based on half size) (+halfSize, -halfSize)
        Vector3 bottomRight = new Vector3(origin.x + halfSize, 0, origin.z - halfSize);
        //Bottom left point (Based on half size) (-halfSize, -halfSize)
        Vector3 bottomLeft = new Vector3(origin.x - halfSize, 0, origin.z - halfSize);

        //run the function to start vertex creation
        createVertices(topRight, topLeft, bottomRight, bottomLeft);
    }

    private void createVertices(Vector3 topRight, Vector3 topLeft, Vector3 bottomRight, Vector3 bottomLeft)
    {
        vertices = new Vector3[4]; //Make the array fit '4' Vector3s, one Vector3 for each vertex
        //Store the points inside of the vertices array
        vertices[0] = topRight;     //Vertex '0'
        vertices[1] = topLeft;      //Vertex '1'
        vertices[2] = bottomRight;  //Vertex '2'
        vertices[3] = bottomLeft;   //Vertex '3'
    }

    private void createTriangles()
    {
        //Size the triangles array to fit the required points for each triangle
        //'6' is there because we want 2 triangles, and it's 3 points / triangle
        triangles = new int[6];
        //Triangle '1'
        triangles[0] = 1; //'1' is the element of the vertices array
        triangles[1] = 0; //'0' is the element of the vertices array
        triangles[2] = 2; //'2' is the element of the vertices array
        //Triangle '2'
        triangles[3] = 1; //'1' is the element of the vertices array
        triangles[4] = 2; //'2' is the element of the vertices array
        triangles[5] = 3; //'3' is the element of the vertices array
    }

    private void createMesh()
    {
        Mesh createdMesh = new Mesh(); //We make a new mesh to store our data
        //Supply the vertices we made to this new mesh
        createdMesh.vertices = vertices;
        //Supply the triangles we made to this new mesh
        createdMesh.triangles = triangles;

        //Get the meshfilter from the gameobject
        MeshFilter filter = GetComponent<MeshFilter>();
        //Supply the new mesh we made to the filter
        filter.mesh = createdMesh;
    }

	// Use this for initialization
	void Start () {
        createPoints();     //Make the points and verts we need
        createTriangles();  //Construct the triangles to represent the shape
        createMesh();       //Create a mesh based on our points and triangles
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    
}
