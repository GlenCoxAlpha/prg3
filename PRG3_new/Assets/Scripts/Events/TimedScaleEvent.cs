﻿using UnityEngine;
using System.Collections;

public class TimedScaleEvent : MonoBehaviour {

	public GameObject target;
	public float shrinkSpeed;
	public float countDown;
	public float scale;
//	public float originalScale = 1;

	void OnTriggerEnter()
	{
		startEvent();
	}

	public void startEvent()
	{
		//target.transform.localScale = originalScale;
		scale = target.transform.localScale.x;
		runEvent();
	}

	void runEvent()
	{
		StartCoroutine (shrinkTarget());
	}

	public void endEvent()
	{
		StopCoroutine(shrinkTarget());
	//	StopAllCoroutines();
	}

	IEnumerator shrinkTarget()
	{
		while(target.transform.localScale.x >0.2)
		{
			Vector3 newscale = target.transform.localScale;
			newscale.x -= 0.9f * Time.deltaTime;
			newscale.y -= 0.9f * Time.deltaTime;
			newscale.z -= 0.9f * Time.deltaTime;

			target.transform.localScale = newscale;
		}
		yield return null;
	}
}
