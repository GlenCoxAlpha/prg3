﻿/*using UnityEngine;
using System.Collections;

public class TimedDamageEvent : MonoBehaviour {

	//when player enters zone  start event
	//deal damage to player every 0.5secs and make player red
	//end if: player leaves the zone, health <0, stop event
	public GameObject target;
	public int damageAmount;
	public Color originalColour = Color.white;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			target = other.gameObject;
			startEvent();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Player")
		{
			target = null;
			endEvent();
		}
	}

	public void startEvent()
	{
		runEvent();
	}

	void runEvent()
	{
		StartCoroutine(dealDamage());
	}
	
	public void endEvent()
	{
		StopCoroutine(dealDamage());
		target.GetComponent<Renderer>().material.color = originalColour;
	}

	IEnumerator dealDamage()
	{
		target.GetComponent<Renderer>().material.color = Color.red;
		target.GetComponent<SimpleHealth>().takeDamage(damageAmount);
		yield return new WaitForSeconds(0.5);
		if(target.GetComponent<SimpleHealth>().health>0)
		{
			dealDamage();
		}
	}
} */