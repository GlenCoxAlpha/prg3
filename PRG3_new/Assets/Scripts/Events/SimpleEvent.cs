﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SimpleEvent : MonoBehaviour {

	public GameObject[] objects;
	private Color colour = Color.yellow;

	void OnTriggerEnter()
	{
		print ("trigger");
		startEvent();
	}

	public void startEvent()
	{
		print ("starting event");
		runEvent ();
	}

	public void runEvent()
	{
		print ("running event");

		foreach(GameObject go in objects)
		{
			go.GetComponent<Renderer>().material.color = colour;
		}
	//	Renderer rend = GetComponentInChildren<Renderer>();
	//	rend.material.color = Color.yellow;
		endEvent ();
	}

	public void endEvent()
	{
		print ("event is over");
	}
}
