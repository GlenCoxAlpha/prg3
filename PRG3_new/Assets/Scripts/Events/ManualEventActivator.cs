﻿using UnityEngine;
using System.Collections;

public class ManualEventActivator : MonoBehaviour {

	private SimpleEvent simpleEvent;
	private TimedEvent timedEvent;
	private TimedScaleEvent timedScaleEvent;

	//if alpha1 pressed activate simple event
	void checkKeys()
	{
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			simpleEvent.startEvent();
		}
		//if alpha2 pressed activate timed event
		if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			timedEvent.startEvent();
		}
	}
	void Update () 
	{
		checkKeys();
	}
}
