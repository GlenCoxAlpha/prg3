﻿using UnityEngine;
using System.Collections;
using System;


public class ParticleSpawner : MonoBehaviour {

	//must store all particels created so it can later destroy them

	public int amountToSpawn;
	public Vector3 positionToSpawn;
	public GameObject particle; //take a prefab

	public void setSpawnProperties()
	{
		//can set amount, position
		//ParticleSystem particleSys = GetComponent<ParticleSystem>();
		amountToSpawn = 5;
		positionToSpawn = new Vector3(0,10,0);
	}

	public void spawnParticles()
	{
		//should spawn an amount based on 
		//at the position that has been specified
		for (int i = 0; i< amountToSpawn; i++)
		{
			Instantiate(particle, positionToSpawn, Quaternion.identity);
		}
	}

	public void destroyParticles()
	{
		//destroy all particle systems it has spawned
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
