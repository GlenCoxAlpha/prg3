﻿using UnityEngine;
using System.Collections;

public class SimpleWorldEvent : MonoBehaviour {

	TimedScaleEvent timedScaleEvent;
	TimedEvent timedevent;
	SimpleEvent simpleEvent;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			startEvent ();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Player")
		{
			endEvent();
		}
	}

	void startEvent()
	{
		//when the player enters a zone make all events activate 
		timedScaleEvent.startEvent();
		timedevent.startEvent();
		simpleEvent.startEvent ();
		runEvent();
	}

	void runEvent()
	{
		//while active remain on standby, do nothing
	}

	void endEvent()
	{
		//end all events
				timedScaleEvent.endEvent();
		timedevent.endEvent();
		simpleEvent.endEvent ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
