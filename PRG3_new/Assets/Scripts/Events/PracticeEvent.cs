﻿using UnityEngine;
using System.Collections;

public class PracticeEvent : MonoBehaviour {

	public bool isColour = false;
	public bool isFreeze = false;
	public Renderer rend;
	private Color red = Color.red;

	void freezePlayer()
	{
		print ("freezing player");
	}

	void changeColour(GameObject target)
	{
		print ("changed colour. really");
	}

	void OnTriggerEnter(Collider other)
	{
		float forceToAdd = 10;
		print ("triggered event");
		//get others physics component and apply force up(impulse)
		other.attachedRigidbody.AddForce(Vector3.up * forceToAdd, ForceMode.Impulse);

		if(isColour)
		{
			print("calling change colour");
			changeColour(other.gameObject);
		}
		else if(isFreeze)
		{
			freezePlayer ();
		}
	}

	void Start () 
	{

	}
	
	void FixedUpdate () 
	{
		
	}
}
