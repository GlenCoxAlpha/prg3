﻿using UnityEngine;
using System.Collections;

public class EventRegister : MonoBehaviour {

	public TimedEvent colourEvent;
	public BasicEvent eventSystem;
	public ParticleSpawner partSpawner;

	void Start () 
	{
		eventSystem.onStartEvent = colourEvent.startEvent;
		eventSystem.onEndEvent = colourEvent.endEvent;
		eventSystem.onStartEvent = partSpawner.setSpawnProperties;

		eventSystem.onRunEvent = partSpawner.spawnParticles;

		eventSystem.onEndEvent = partSpawner.destroyParticles;
	}

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			eventSystem.startEvent();
		}
	}
}
