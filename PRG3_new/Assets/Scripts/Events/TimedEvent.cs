﻿using UnityEngine;
using System.Collections;

public class TimedEvent : MonoBehaviour {

	public Color originalColour = Color.white;
	public float countDown;
	public GameObject target;

	void OnTriggerEnter(Collider other)
	{
		//when player enters a zone 
		startEvent();
	}
	public void startEvent()
	{
		originalColour = target.GetComponent<Renderer>().material.color;
		countDown = 3;
		//start 3sec countdown
		runEvent ();
	}

	void runEvent()
	{
		//while counting down make a gameobject get a random colour constantly
		StartCoroutine (changeColour());
	}

	public void endEvent()
	{
		//when finished return back to origianl colour
		target.GetComponent<Renderer>().material.color = originalColour;
		//reset timer
	}

	IEnumerator changeColour()
	{
		while (countDown>0)
		{
			countDown -= Time.deltaTime;
			yield return null;
			//change colour here
			target.GetComponent<Renderer>().material.color 
				= new Color (Random.value, Random.value,Random.value);
		}
		endEvent ();
	}
}
