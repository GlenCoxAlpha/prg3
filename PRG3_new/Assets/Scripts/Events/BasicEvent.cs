﻿using UnityEngine;
using System.Collections;

public class BasicEvent : MonoBehaviour {

	public delegate void EventDelegate();
	public EventDelegate onStartEvent,onRunEvent, onEndEvent;

	public void startEvent()
	{
		if(onStartEvent != null)
		{
			//anythign required to happen before the event begins
			onStartEvent();
		}
		runEvent();
	}

	public void runEvent()
	{
		if(onRunEvent !=null)
		{
			onRunEvent();
		}
	}

	public void endEvent()
	{
		if(onEndEvent !=null)
		{
			onEndEvent();
		}

	}
}
