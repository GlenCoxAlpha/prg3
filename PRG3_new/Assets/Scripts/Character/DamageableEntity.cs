﻿using UnityEngine;
using System.Collections;

public class DamageableEntity : MonoBehaviour {

	public float health;
	public bool isDead;
	public modifyHealthDelegate onModifyHealth;
	public delegate void modifyHealthDelegate();

	public virtual void Start()
	{
		if(health <= 0)
		{
			isDead = true;
		}
	}

	public void modifyHealth(float amount)
	{
		health += amount;
		//we need a callback function to run when an entity takes damage
		if(amount<0)
		{
			//damage is being applied
			if(onModifyHealth != null) //if its not null
			{
				onModifyHealth();
			}
			else
			{
				Debug.LogWarning(this + ": onModifyHealth delegate is not assigned" );
			}
		}
		else
		{
			//healing is being applied
			if(onModifyHealth !=null)
			{
				onModifyHealth();
			}
			else
			{
				Debug.LogWarning(this + "onmodifyHealth delegate is not assigned");
			}
		}

		if(health <= 0)
		{
			kill ();
		}
	}

	public void kill()
	{
		health = 0;
		isDead = true;
		//callback function when an entity dies
	}
}
