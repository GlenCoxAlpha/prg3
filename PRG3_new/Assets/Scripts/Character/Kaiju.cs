﻿using UnityEngine;
using System.Collections;

public class Kaiju : DamageableEntity 
{

	private void debugControls()
	{
		if(Input.GetKeyDown(KeyCode.Minus))
		{
			takeDamage(1);
			print ("minus key");
		}

		if(Input.GetKeyDown(KeyCode.Plus))
		{
			heal(1);
			print ("plus key");
		}
	}

	public override void Start()
	{
		base.Start();
	}

	protected void onTakeDamage()
	{
		print ("play take damage animation");
	}

	protected void onHeal()
	{
		print ("healing kaiju");
	}

	public void heal(float amount)
	{
		amount = Mathf.Abs(amount); //absolute value of something means positive
		//we know we are healing so we should assign a 'onHeal' function to onModifyHealth
		onModifyHealth = onHeal;
		modifyHealth(amount);
	}

	public void takeDamage(float amount)
	{
		amount = Mathf.Abs (amount) * -1; //make it positive then flip it
		//we know we are damaged so we should assign a 'onTakeDamage' function to onModifyHealth
		onModifyHealth = onTakeDamage;
		modifyHealth(amount);
	}

	void Update () 
	{
		
	}
}
