﻿using UnityEngine;
using System.Collections;

public abstract class KaijuController : MonoBehaviour {

    public KaijuAnimator kaijuAnims;
    public KaijuMover kaijuMover;

    protected void stopWalkFunction()
    {
        kaijuAnims.setWalkState(false);
    }

    protected void walkFunction()
    {
        //make the kaiju walk (move)
        kaijuMover.walk();
        //make the kaiju animate walking (anims)
        kaijuAnims.setWalkState(true);
    }

    protected void primaryAttackFunction()
    {
        if (!kaijuAnims.isAttacking())
        {
            //toggle any damage affectors (don't have any yet)
            //make the kaiju animate primary attack (anims)
            kaijuAnims.playAttack();
        }
    }

    protected void specialAttackFunction()
    {
        if(!kaijuAnims.isAttacking())
        {
            //toggle any damage affectors (don't have any yet)
            //make the kaiju animate special attack (anims)
            kaijuAnims.playSpecialAttack();
        }
    }

    protected void turnFunction(int direction)
    {
        //make the kaiju turn (rotate)
        kaijuMover.turn(direction);
        //make the kaiju animate turn?/walk (anims)
        kaijuAnims.setWalkState(true);
    }
}
