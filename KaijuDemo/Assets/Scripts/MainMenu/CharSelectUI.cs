﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharSelectUI : MonoBehaviour {

    public CharacterSelection selector;
    public Button[] selectionButtons;
    public Button playButton;

    public void highlightSelectedKaiju()
    {
        foreach (Button b in selectionButtons)
        {
            if (selector.figureOutKaiju(b.name) == selector.selectedKaiju)
            {
                b.image.color = Color.green;
            }
            else
            {
                b.image.color = Color.white;
            }
        }

        playButton.interactable = true;
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
